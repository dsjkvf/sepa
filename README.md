sepa
====

## About

`sepa` is a simple script to split the provided file by the provided pattern. The new files will be saved as as a group of `[LineNumber][LineText].part` files.

## Usage

The general rule is the following:

    sepa <FILE> <PATTERN>

For example, this command will split the `~/.bashrc` file into smaller parts by its capitalized comment lines:

    sepa ~/.bashrc '^# [A-Z][a-z]'
